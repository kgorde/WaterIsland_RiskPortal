# Generated by Django 2.0.6 on 2018-12-10 10:52

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('risk', '0006_auto_20181207_1711'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='ess_idea',
            name='peers',
        ),
        migrations.AddField(
            model_name='ess_idea',
            name='peers',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='risk.ESS_Peers'),
        ),
    ]
