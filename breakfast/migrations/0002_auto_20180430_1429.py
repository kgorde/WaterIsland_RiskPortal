# Generated by Django 2.0.3 on 2018-04-30 14:29

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('breakfast', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='breakfastorder',
            name='user',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
        ),
    ]
