# Generated by Django 2.0.6 on 2018-11-27 14:16

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('risk_reporting', '0022_auto_20181127_1412'),
    ]

    operations = [
        migrations.AlterField(
            model_name='formulaebaseddownsides',
            name='IsExcluded',
            field=models.CharField(max_length=22),
        ),
    ]
