# Generated by Django 2.0.6 on 2018-11-01 14:56

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('risk_reporting', '0006_arbnavimpacts'),
    ]

    operations = [
        migrations.AlterField(
            model_name='arbnavimpacts',
            name='Capital',
            field=models.FloatField(null=True),
        ),
    ]
