# Generated by Django 2.0.6 on 2018-10-23 13:29

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('risk_reporting', '0002_auto_20181023_1326'),
    ]

    operations = [
        migrations.AlterField(
            model_name='riskattributes',
            name='risk_limit',
            field=models.CharField(max_length=10),
        ),
    ]
